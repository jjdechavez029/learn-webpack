function greeter(person: string) {
    return `Hello ${person}`;
}

let user = 'John Doe';

console.log(greeter(user));